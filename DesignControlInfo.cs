using System;
using System.IO;
using System.Collections.Generic;
using System.Reflection;
using System.Windows.Forms;

namespace WinFormsDesigner.CSharp.Mono
{
    public class DesignControlInfo
    {
        public string ctrlName = "";
        public string ctrlClassName = "";
        public List<string> propertyName = new List<string>();
        public List<string> strProperty = new List<string>();
        public List<string> addCtrlName = new List<string>();
        public List<string> subAdd_CtrlName = new List<string>();
        public List<string> subAdd_childCtrlName = new List<string>();
        public List<string> subAddRange_CtrlName = new List<string>();
        public List<string> subAddRange_childCtrlName = new List<string>();
        public List<string> decHandler = new List<string>();

        public DesignControlInfo() { }
    }
}
