﻿using System;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace WinFormsDesigner.CSharp.Mono
{
    internal class DesignerTreeNode : TreeNode
    {
        private DesignerTreeNode[] itemNode = new DesignerTreeNode[] {};//Array.Empty<DesignerTreeNode>();

        public DesignerTreeNode(string nodeName)
        {
            try
            {
                this.Text = nodeName;
    
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }
        }

        internal DesignerTreeNode Search(string name)
        {
            DesignerTreeNode returnValue = null;

            try
            {
                if (Text == name)
                {
                    returnValue = this;
                }
                else
                {
                    for (int i = 0; i < itemNode.Count(); i++)
                    {
                        if (itemNode[i].Search(name) != null)
                        {
                            returnValue = itemNode[i].Search(name);
                            break;
                        }
                    }
                }
    
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }
            return returnValue;
        }

        internal void Add(string name, string className)
        {
            try
            {
                Array.Resize(ref itemNode, itemNode.Count() + 1);
                if (className == "SplitContainer")
                {
                    itemNode[itemNode.Count() - 1] = new DesignerTreeNode(name + ".Panel1");
                    Array.Resize(ref itemNode, itemNode.Count() + 1);
                    itemNode[itemNode.Count() - 1] = new DesignerTreeNode(name + ".Panel2");
                }
                else
                {
                    itemNode[itemNode.Count() - 1] = new DesignerTreeNode(name);
                }
    
                this.Nodes.Clear();
                this.Nodes.AddRange(itemNode);
                this.Expand();
    
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }
        }
    }
}
