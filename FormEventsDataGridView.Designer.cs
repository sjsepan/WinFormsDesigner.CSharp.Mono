﻿namespace WinFormsDesigner.CSharp.Mono
{
    partial class FormEventsDataGridView
    {
        /// <summary>
        /// Required designer variables.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources that are in use.
        /// </summary>
        /// <param name="disposing">Specify true to destroy managed resources, or false otherwise.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code generated in Component Designer

        /// <summary>
        /// Required methods for designer support. The contents of this method are 
        /// Do not change it in the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
        }

        #endregion
    }
}
