﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Reflection;

namespace WinFormsDesigner.CSharp.Mono
{
    public class DesignerCanvasForm : Panel
    {
        public MainForm mainForm;
        public List<DesignerControls> CtrlItems = new List<DesignerControls>();
        private DesignerControlSelectedBox selectBox;
        private bool selectFlag = false;
        public Form memForm = new Form();
        public TabPage backPanel;
        private int grid = 8;
        public List<string> decHandler = new List<string>();
        public List<string> decFunc = new List<string>();

        // ****************************************************************************************
        // Edit the following when adding a control
        // ****************************************************************************************
        public int cnt_Control = -1;
        public int cnt_Button;
        public int cnt_Label;
        public int cnt_TextBox;
        public int cnt_ListBox;
        public int cnt_GroupBox;
        public int cnt_TabControl;
        public int cnt_TabPage;
        public int cnt_CheckBox;
        public int cnt_ComboBox;
        public int cnt_SplitContainer;
        public int cnt_DataGridView;
        public int cnt_Panel;
        public int cnt_CheckedListBox;
        public int cnt_LinkLabel;
        public int cnt_PictureBox;
        public int cnt_ProgressBar;
        public int cnt_RadioButton;
        public int cnt_RichTextBox;
        public int cnt_StatusStrip;
        public int cnt_ListView;
        public int cnt_TreeView;
        public int cnt_MonthCalendar;
        public int cnt_HScrollBar;
        public int cnt_VScrollBar;
        public int cnt_MaskedTextBox;
        public int cnt_PropertyGrid;
        public int cnt_DateTimePicker;
        public int cnt_DomainUpDown;
        
        private void CountInit()
        {
            try
            {
                cnt_Control = -1;
                cnt_Button = 0;
                cnt_Label = 0;
                cnt_TextBox = 0;
                cnt_ListBox = 0;
                cnt_GroupBox = 0;
                cnt_TabControl = 0;
                cnt_TabPage = 0;
                cnt_CheckBox = 0;
                cnt_ComboBox = 0;
                cnt_SplitContainer = 0;
                cnt_DataGridView = 0;
                cnt_Panel = 0;
                cnt_CheckedListBox = 0;
                cnt_LinkLabel = 0;
                cnt_PictureBox = 0;
                cnt_ProgressBar = 0;
                cnt_RadioButton = 0;
                cnt_RichTextBox = 0;
                cnt_StatusStrip = 0;
                cnt_ListView = 0;
                cnt_TreeView = 0;
                cnt_MonthCalendar = 0;
                cnt_HScrollBar = 0;
                cnt_VScrollBar = 0;
                cnt_MaskedTextBox = 0;
                cnt_PropertyGrid = 0;
                cnt_DateTimePicker = 0;
                cnt_DomainUpDown = 0;
    
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }
        }
        // ****************************************************************************************

        public void Init(MainForm mainForm, TabPage backPanel)
        {
            try
            {
                this.mainForm = mainForm;
                this.backPanel = backPanel;
                this.Click += new System.EventHandler(Form_Click);
                this.Resize += new System.EventHandler(formResize);
                backPanel.Click += new System.EventHandler(Backpanel_Click);
    
                mainForm.propertyGrid.PropertyValueChanged += new System.Windows.Forms.PropertyValueChangedEventHandler(PropertyValueChanged);
                memForm.Location = this.Location;
                memForm.ClientSize = this.Size;
                memForm.Name = "Form1";
    
                selectBox = new DesignerControlSelectedBox(this, backPanel);
                SetSelect(true);
    
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }
        }
        private void Backpanel_Click(object sender, EventArgs e)
        {
            try
            {
                MouseEventArgs me = (MouseEventArgs)e;
    
                if (me.Button == MouseButtons.Left)
                {
                    SelectAllClear();
                }
    
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }
        }
        private void PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            try
            {
                Control ctrl = mainForm.propertyGrid.SelectedObject as Control;
    
                if (ctrl.Name == memForm.Name)
                {
                    string[] split = e.ChangedItem.ToString().Split(' ');
                    PropertyInfo item;
    
                    string propertyName = split[1];
                    if (propertyName == "Size")
                    {
                        item = memForm.GetType().GetProperty("ClientSize");
                    }
                    else
                    {
                        item = memForm.GetType().GetProperty(propertyName);
                    }
    
                    PropertyInfo formItem = this.GetType().GetProperty(propertyName);
    
                    if (formItem != null)
                    {
                        formItem.SetValue(this, item.GetValue(memForm));
                        SetSelect(true);
                    }
                }
                else
                {
                    for (int i = 0; i < CtrlItems.Count; i++)
                    {
                        if (CtrlItems[i].ctrl.Name == ctrl.Name)
                        {
                            CtrlItems[i].Selected = true;
                        }
                    }
                }
    
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }
        }

        private void formResize(object sender, EventArgs e)
        {
            try
            {
                memForm.ClientSize = this.Size;
    
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }
        }
        private void Form_Click(object sender, EventArgs e)
        {
            try
            {
                MouseEventArgs me = (MouseEventArgs)e;
    
                if (mainForm.toolLstBox.Text == "")
                {
                    if (me.Button == MouseButtons.Left)
                    {
                        if (selectFlag)
                        {
                            SetSelect(false);
                        }
                        else
                        {
                            SelectAllClear();
                            SetSelect(true);
                        }
                    }
                }
                else
                {
                    SelectAllClear();
    
                    int X = (int)(me.X / grid) * grid;
                    int Y = (int)(me.Y / grid) * grid;
                    _ = new DesignerControls(this, mainForm.toolLstBox.Text, this, X, Y);
                    mainForm.toolLstBox.SelectedIndex = -1;
                }
    
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }
        }
        private void Delete(DesignerControls ctrl)
        {
            try
            {
                for (int i = 0; i < CtrlItems.Count; i++)
                {
                    if (ctrl.ctrl == CtrlItems[i].ctrl.Parent)
                    {
                        Delete(CtrlItems[i]);
                        i--;
                    }
    
                    if (ctrl.ctrl is SplitContainer)
                    {
                        SplitContainer splcontainer = ctrl.ctrl as SplitContainer;
    
                        for (int j = 0; j < CtrlItems.Count; j++)
                        {
                            if (splcontainer.Panel1 == CtrlItems[j].ctrl.Parent || splcontainer.Panel2 == CtrlItems[j].ctrl.Parent)
                            {
                                Delete(CtrlItems[j]);
                                i--;
                            }
                        }
                    }
                }
                ctrl.Delete();
                CtrlItems.Remove(ctrl);
    
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }
        }
        public void RemoveSelectedItem()
        {
            try
            {
                for (int i = 0; i < CtrlItems.Count; i++)
                {
                    if (CtrlItems[i].Selected)
                    {
                        if (CtrlItems[i].ctrl is TabPage)
                        {
                            TabControl tabctrl = CtrlItems[i].ctrl.Parent as TabControl;
    
                            if (tabctrl.TabPages.Count > 1)
                            {
                                Delete(CtrlItems[i]);
                                i--;
                            }
                        }
                        else
                        {
                            Delete(CtrlItems[i]);
                            i--;
                        }
                    }
                }
    
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }
        }
        public void CtrlAllClear()
        {
            try
            {
                CountInit();
    
                for (int i = 0; i < CtrlItems.Count; i++)
                {
                    CtrlItems[i].Selected = true;
                }
                RemoveSelectedItem();
    
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }
        }
        public void Add_Controls(List<DesignControlInfo> ctrlInfo)
        {
            try
            {
                // Delete Full Control
                CtrlAllClear();
    
                // Control Done
                for (int i = 0; i < ctrlInfo.Count; i++)
                {
                    _ = new DesignerControls(this, memForm, ctrlInfo[i]);
                }
                this.Location = new Point(16, 16);
    
                // Other settings
    
                for (int i = 0; i < ctrlInfo.Count; i++)
                {
                    for (int j = 0; j < CtrlItems.Count; j++)
                    {
                        if (CtrlItems[j].ctrl != null)
                        {
                            if (CtrlItems[j].ctrl.Name == ctrlInfo[i].ctrlName || ctrlInfo[i].ctrlName == "this")
                            {
                                CtrlItems[j].SetControls(ctrlInfo[i]);
                            }
                        }
                    }
                }
                // selectbox establishment
                for (int j = 0; j < CtrlItems.Count; j++)
                {
                    CtrlItems[j].InitSelectBox();
                }
    
                SelectAllClear();
    
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }
        }
        public void SetSelect(bool flag)
        {
            try
            {
                selectFlag = flag;
                selectBox.SetSelectBoxPos(selectFlag);
                ShowProperty(flag);
                if (flag)
                {
                    mainForm.ctrlTree.SelectedNode = mainForm.ctrlTree.TopNode;
                }
                mainForm.eventView.ShowEventList(flag, this);
    
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }
        }
        public void SelectAllClear()
        {
            try
            {
                SetSelect(false);
    
                for (int i = 0; i < CtrlItems.Count; i++)
                {
                    CtrlItems[i].Selected = false;
                }
    
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }
        }
        private void ShowProperty(bool flag)
        {
            try
            {
                if (flag)
                {
                    mainForm.propertyGrid.SelectedObject = this.memForm;
                    mainForm.propertyCtrlName.Text = this.memForm.Name;
                }
                else
                {
                    mainForm.propertyGrid.SelectedObject = null;
                    mainForm.propertyCtrlName.Text = "";
                }
    
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }
        }
    }
}
