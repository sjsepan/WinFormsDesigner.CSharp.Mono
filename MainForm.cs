using System;
// using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
// using System.Data;
using System.Drawing;
// using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;

namespace WinFormsDesigner.CSharp.Mono
{
    public partial class MainForm : Form
    {
        #region Declarations
        private const String ACTION_IN_PROGRESS = " ...";
        private const String ACTION_CANCELLED = " cancelled";
        private const String ACTION_DONE = " done";
        
        #region model //TODO: need to build model from this
        private DesignFileInfo fileInfo;
        internal PropertyGrid propertyGrid;
        internal TextBox propertyCtrlName;
        internal ListBox toolLstBox;
        internal TreeView ctrlTree;
        internal FormEventsDataGridView eventView;
        private string sourceFileName = "";
        #endregion model
        #endregion Declarations

        #region Constructors
        public MainForm()
        {
            try
            {
                InitializeComponent();

                Private2Internal_Controls();
                userForm.Init(this, designePage);
                DesignerControls.AddToolList(ctrlLstBox);
                DesignerFile.ReadIni(this, "WinFormsDesigner.CSharp.Mono.ini", mainWndSplitContainer, subWndSplitContainer);
                Run_CommandLine();//opens passed file, or does new
                
                //TODO:hard-code; or replace with window-close
                Application.ApplicationExit += new EventHandler(AppExit);
    
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }
        }
        #endregion Constructors
        
        #region Events
        #region Form
        private void MainForm_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Alt && e.KeyCode == Keys.Delete && designeTab.SelectedIndex == 0)
                {
                    userForm.RemoveSelectedItem();
                }
    
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }
        }
        #endregion Form

        #region Menu
        private void menuFileNew_Click(object sender, EventArgs e)
        {
            try
            {//TODO:check for dirty and save
                //new file;app already knows how to do this as fallback during open
                fileInfo = DesignerFile.NewFile();
                PresentDesignFileInfo(fileInfo);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }
        }
        private void menuFileOpen_Click(object sender, EventArgs e)
        {
            try
            {//TODO:check for dirty and save
                fileInfo = DesignerFile.OpenFile();
                PresentDesignFileInfo(fileInfo);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }
        }
        private void menuFileSave_Click(object sender, EventArgs e)
        {
            try
            {
                designeTab.SelectedIndex = 0;
                designeTab.SelectedIndex = 1;
    
                if (sourceFileName == "")
                {
                    throw new ApplicationException("sourceFileName not set");
                }
                
                if (sourceFileName != DesignerFile.FILE_NEW + "." + DesignerFile.FILE_EXT)
                {
                    DesignerFile.SaveAs(sourceFileName, sourceTxtBox.Text);
                }
                else
                {
                    DesignerFile.Save(sourceTxtBox.Text);
                }
                // Close();
    
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }
        }
        private async void menuFileClose_Click(object sender, EventArgs e)
        {
            try
            {
                //TODO:close file only, but not app
                StatusBarStatusMessage.Text = "Close" + ACTION_IN_PROGRESS;
                StartProgressBar();
                // StartActionIcon(menuFileClose.Image); 
                await FileCloseAction();
                // StopActionIcon(); 
                StopProgressBar();
                StatusBarStatusMessage.Text += ACTION_DONE;
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }
        }
        private void menuFileQuit_Click(object sender, EventArgs e)
        {
            try
            {
                Close();
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }
        }
        // private async void MenuEditUndo_Click(object sender, EventArgs e)
		// {
        //     StatusBarStatusMessage.Text = "Undo" + ACTION_IN_PROGRESS;
        //     StartProgressBar();
        //     StartActionIcon(menuEditUndo.Image); 
        //     await EditUndoAction();
        //     StopActionIcon(); 
        //     StopProgressBar();
        //     StatusBarStatusMessage.Text += ACTION_DONE;
        // }
        // private async void MenuEditRedo_Click(object sender, EventArgs e)
		// {
        //     StatusBarStatusMessage.Text = "Redo" + ACTION_IN_PROGRESS;
        //     StartProgressBar();
        //     StartActionIcon(menuEditRedo.Image); 
        //     await EditRedoAction();
        //     StopActionIcon(); 
        //     StopProgressBar();
        //     StatusBarStatusMessage.Text += ACTION_DONE;
        // }
        private async void MenuEditSelectAll_Click(object sender, EventArgs e)
		{
            StatusBarStatusMessage.Text = "Select All" + ACTION_IN_PROGRESS;
            StartProgressBar();
            // StartActionIcon("SelectAll"); 
            await EditSelectAllAction();
            // StopActionIcon(); 
            StopProgressBar();
            StatusBarStatusMessage.Text += ACTION_DONE;
        }
        private async void MenuEditCut_Click(object sender, EventArgs e)
		{
            StatusBarStatusMessage.Text = "Cut" + ACTION_IN_PROGRESS;
            StartProgressBar();
            StartActionIcon(menuEditCut.Image); 
            await EditCutAction();
            StopActionIcon(); 
            StopProgressBar();
            StatusBarStatusMessage.Text += ACTION_DONE;
        }
        private async void MenuEditCopy_Click(object sender, EventArgs e)
		{
            StatusBarStatusMessage.Text = "Copy" + ACTION_IN_PROGRESS;
            StartProgressBar();
            StartActionIcon(menuEditCopy.Image); 
            await EditCopyAction();
            StopActionIcon(); 
            StopProgressBar();
            StatusBarStatusMessage.Text += ACTION_DONE;
        }
        private async void MenuEditPaste_Click(object sender, EventArgs e)
		{
            StatusBarStatusMessage.Text = "Paste" + ACTION_IN_PROGRESS;
            StartProgressBar();
            StartActionIcon(menuEditPaste.Image); 
            await EditPasteAction();
            StopActionIcon(); 
            StopProgressBar();
            StatusBarStatusMessage.Text += ACTION_DONE;
        }
        private async void MenuEditPasteSpecial_Click(object sender, EventArgs e)
		{
            StatusBarStatusMessage.Text = "Paste Special" + ACTION_IN_PROGRESS;
            StartProgressBar();
            // StartActionIcon("PasteSpecial"); 
            await EditPasteSpecialAction();
            // StopActionIcon(); 
            StopProgressBar();
            StatusBarStatusMessage.Text += ACTION_DONE;
        }
        private void MenuEditDelete_Click(object sender, EventArgs e)
        {
            StatusBarStatusMessage.Text = "Delete" + ACTION_IN_PROGRESS;
            StartProgressBar();
            StartActionIcon(menuEditDelete.Image); 
            userForm.RemoveSelectedItem();
            // await EditDeleteAction();
            StopActionIcon(); 
            StopProgressBar();
            StatusBarStatusMessage.Text += ACTION_DONE;
        }
        // private async void MenuEditFind_Click(object sender, EventArgs e)
		// {
        //     StatusBarStatusMessage.Text = "Find" + ACTION_IN_PROGRESS;
        //     StartProgressBar();
        //     StartActionIcon(menuEditFind.Image); 
        //     await EditFindAction();
        //     StopActionIcon(); 
        //     StopProgressBar();
        //     StatusBarStatusMessage.Text += ACTION_DONE;
        // }
        // private async void MenuEditReplace_Click(object sender, EventArgs e)
		// {
        //     StatusBarStatusMessage.Text = "Replace" + ACTION_IN_PROGRESS;
        //     StartProgressBar();
        //     StartActionIcon(menuEditReplace.Image); 
        //     await EditReplaceAction();
        //     StopActionIcon(); 
        //     StopProgressBar();
        //     StatusBarStatusMessage.Text += ACTION_DONE;
        // }
        // private async void MenuEditRefresh_Click(object sender, EventArgs e)
		// {
        //     StatusBarStatusMessage.Text = "Refresh" + ACTION_IN_PROGRESS;
        //     StartProgressBar();
        //     StartActionIcon(menuEditRefresh.Image); 
        //     await EditRefreshAction();
        //     StopActionIcon(); 
        //     StopProgressBar();
        //     StatusBarStatusMessage.Text += ACTION_DONE;
        // }
        private async void MenuEditPreferences_Click(object sender, EventArgs e)
		{
            StatusBarStatusMessage.Text = "Preferences" + ACTION_IN_PROGRESS;
            StartProgressBar();
            StartActionIcon(menuEditPreferences.Image); 
            await EditPreferencesAction();
            StopActionIcon(); 
            StopProgressBar();
            StatusBarStatusMessage.Text += ACTION_DONE;
        }
        // private async void MenuEditProperties_Click(object sender, EventArgs e)
		// {
        //     StatusBarStatusMessage.Text = "Properties" + ACTION_IN_PROGRESS;
        //     StartProgressBar();
        //     StartActionIcon(menuEditProperties.Image); 
        //     await EditPropertiesAction();
        //     StopActionIcon(); 
        //     StopProgressBar();
        //     StatusBarStatusMessage.Text += ACTION_DONE;
        // }
        private async void MenuHelpAbout_Click(object sender, EventArgs e)
        {
            await HelpAboutAction();
        }
        #endregion Menu

        #region Controls
        private void designeTab_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Console.WriteLine("designeTab_SelectedIndexChanged:"+designeTab.SelectedIndex.ToString());
                switch (designeTab.SelectedIndex)
                {//BUG:"Object reference not set to an instance of an object" selecting 'Source'
                    case 0://Design
                        break;
                    case 1://Source
                        Console.WriteLine("fileInfo:"+(fileInfo==null).ToString());
                        // Console.WriteLine("DesignerFile:"+(DesignerFile==null).ToString());//Static
                        // Console.WriteLine("sourceTxtBox:"+(sourceTxtBox==null).ToString());
                        //BUG:if still new, no source saved, and FILEINFO object not instantiated
                        //NOTE: FILEINFO is now a class, not struct, due to version of C# used in Mono
                        if (fileInfo.source_base == null)
                        {
                            fileInfo.source_base = DesignerFile.BlankFile();
                        }
                        sourceTxtBox.Text = Create_SourceCode();
                        break;
                    case 2://Events
                        eventTxtBox.Text = Create_EventCode();
                        break;
                    case 3://Log
                        break;
                }
    
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }
        }
        private void nameTxtBox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (propertyGrid.SelectedObject != null && propertyGrid.SelectedObject is Form == false)
                {
                    Control ctrl = propertyGrid.SelectedObject as Control;
                    ctrl.Name = propertyCtrlName.Text;
                }
    
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }
        }
        private void ctrlsTab_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ctrlsTab.SelectedIndex == 1)
                {
                    Show_ControlViewShow(userForm);
                }
    
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }
        }
        private void ctrlTreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            try
            {
                if (ctrlTree.SelectedNode == null) { return; }
                string ctrlName = ctrlTree.SelectedNode.Text;
    
                if (ctrlName == "Form")
                {
                    userForm.SelectAllClear();
                    userForm.SetSelect(true);
                }
                else
                {
                    for (int i = 0; i < userForm.CtrlItems.Count; i++)
                    {
                        if (userForm.CtrlItems[i].ctrl.Name == ctrlName && !userForm.CtrlItems[i].Selected)
                        {
                            userForm.SelectAllClear();
                            userForm.CtrlItems[i].Selected = true;
                            break;
                        }
                    }
                }
                ctrlTree.Focus();
    
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }
        }
        #endregion Controls
        #endregion Events

        #region Actions
        private async Task DoSomething()
        {
            for (int i = 0; i < 3; i++)
            {
                //StatusBarProgressBar.Pulse();
                //DoEvents;
                await Task.Delay(1000); 
            }
        }
        private async Task FileCloseAction()
        {
            await DoSomething();
        }
        private async Task EditSelectAllAction()
        {
            await DoSomething();
        }
        private async Task EditCutAction()
        {
            await DoSomething();
        }
        private async Task EditCopyAction()
        {
            await DoSomething();
        }
        private async Task EditPasteAction()
        {
            await DoSomething();
        }
        private async Task EditPasteSpecialAction()
        {
            await DoSomething();
        }
        private async Task EditDeleteAction()
        {
            await DoSomething();
        }
        private async Task EditPreferencesAction()
        {
            await DoSomething();
            // //do a folder-path dialog demo for preferences
            // FolderBrowserDialog folderBrowserDialog = null;
            // DialogResult response = DialogResult.None;

            // try
            // {
            //     folderBrowserDialog = new FolderBrowserDialog();
            //     // folderBrowserDialog.Title = this.Title;

            //     response = folderBrowserDialog.ShowDialog(this); 
                
            //     if (response == DialogResult.OK)
            //     {
            //         StatusBarStatusMessage.Text += folderBrowserDialog.SelectedPath + ACTION_IN_PROGRESS;
            //     }
            //     else //if (response == DialogResult.Cancel)
            //     {
            //         StatusBarStatusMessage.Text += ACTION_CANCELLED + ACTION_IN_PROGRESS;
            //     }
            
            //     folderBrowserDialog.Dispose();
            // }
            // catch (Exception ex)
            // {
            //     Console.Error.WriteLine(ex.Message);
            // }
        }
        private async Task HelpAboutAction()
        {
            DialogResult response = DialogResult.None;
            String message = null;

            StatusBarStatusMessage.Text = "About" + ACTION_IN_PROGRESS;
            StartProgressBar();
            StartActionIcon(menuHelpAbout.Image); 

            // aboutDialog.Logo = GetIconFromBitmap(GetBitmapFromFile("../EtoFormsApp2/Resources/App.png"), 32, 32);
            message += "ProgramName = WinFormsDesigner.CSharp.Mono\n";
            message += "Version = 0.1\n";
            message += "Forked from 'Simple WinForms Designer For CSharp' at <https://github.com/hry2566/SWD4CS> and ported to Mono.\n" +
                "Credit to hry2566 for original project\n";
            message += "WebsiteLabel = GitLab\n";
            message += "Website = http://www.gitlab.com\n";
            // // aboutDialog.Platform;r/o
            message += "Copyright = Copyright (C) 2022 Stephen J Sepan\n";
            message += "Designers = Stephen J Sepan\n";
            message += "Developers = Stephen J Sepan\n";
            message += "Documenters = Stephen J Sepan\n";
            message += "License = see LICENSE file in source\n";

            response = MessageBox.Show
            (
                owner: this,
                text: message,
                caption: "About " + this.Text,
                buttons: MessageBoxButtons.OK,
                icon: MessageBoxIcon.Information,
                defaultButton: MessageBoxDefaultButton.Button1
            );

            StopActionIcon(); 
            StopProgressBar();
            StatusBarStatusMessage.Text += ACTION_DONE;
        }
        #endregion Actions

        private void Private2Internal_Controls()
        {
            try
            {
                propertyGrid = propertyBox;
                propertyCtrlName = nameTxtBox;
                toolLstBox = ctrlLstBox;
                ctrlTree = ctrlTreeView;
                eventView = evtGridView;
    
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }
        }
        private void AppExit(object sender, EventArgs e)
        {
            try
            {
                DesignerFile.WriteIni(this, "WinFormsDesigner.CSharp.Mono.ini", mainWndSplitContainer, subWndSplitContainer);
    
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }
        }
        private void Run_CommandLine()
        {
            try
            {
                string[] cmds = System.Environment.GetCommandLineArgs();
                if (cmds.Length < 2) 
                { 
                    if (fileInfo == null)
                    {
                        fileInfo = new DesignFileInfo();
                    }
                    //TODO:also need to make new FILEINFO on new, OR prevent use of Source tab until saved
                    sourceFileName = fileInfo.source_FileName;
                    logTxtBox.Text = "";
                    userForm.Add_Controls(fileInfo.ctrlInfo);
                }
                else
                {
                    fileInfo = DesignerFile.CommandLine(cmds[1]);//TODO:also need to make new FILEINFO on new, OR prevent use of Source tab until saved
                    sourceFileName = fileInfo.source_FileName;
                    logTxtBox.Text = "";
                    userForm.Add_Controls(fileInfo.ctrlInfo);
                }
    
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }
        }
        internal void Add_Log(string log)
        {
            try
            {
                if (logTxtBox.Text == "")
                {
                    logTxtBox.Text = log;
                }
                else
                {
                    logTxtBox.Text += "\r\n" + log;
                }
    
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }
        }
        private void PresentDesignFileInfo(DesignFileInfo info)
        {
            try
            {
                sourceFileName = info.source_FileName;
                logTxtBox.Text = "";
                userForm.Add_Controls(info.ctrlInfo);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }
        }
        private string Create_EventCode()
        {
            string eventSource = null;
            try
            {
                List<string> decHandler = new List<string>();
                List<string> decFunc = new List<string>();
    
                Add_Declaration(ref decHandler, ref decFunc);
    
                if (decHandler.Count == 0) { return ""; }
    
                if (fileInfo.source_base == null)
                {
                    fileInfo.source_base = DesignerFile.BlankFile();
                }
    
                string[] split = Create_SourceCode().Split(Environment.NewLine.ToCharArray());
                eventSource = Create_EventsSource(split, decHandler, decFunc);
    
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }

            return eventSource;
        }
        private static string Create_EventsSource(string[] split, List<string> decHandler, List<string> decFunc)
        {
            string eventSource = "";

            try
            {
                for (int i = 0; i < 4; i++)
                {
                    if (eventSource == "")
                    {
                        eventSource = split[i];
                    }
                    else
                    {
                        eventSource += Environment.NewLine + split[i];
                    }
                }
    
                eventSource += Environment.NewLine;
                eventSource += "    private void InitializeEvents()" + Environment.NewLine;
                eventSource += "    {" + Environment.NewLine;
    
    
                for (int i = 0; i < decHandler.Count; i++)
                {
                    eventSource += "        " + decHandler[i] + Environment.NewLine;
                }
    
                eventSource += "    }" + Environment.NewLine + Environment.NewLine;
    
                for (int i = 0; i < decFunc.Count; i++)
                {
                    eventSource += "    " + decFunc[i] + Environment.NewLine;
                    eventSource += "    {" + Environment.NewLine + Environment.NewLine;
                    eventSource += "    }" + Environment.NewLine + Environment.NewLine;
                }
    
                eventSource += "}" + Environment.NewLine;
    
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }

            return eventSource;
        }
        private void Add_Declaration(ref List<string> decHandler, ref List<string> decFunc)
        {
            try
            {
                for (int i = 0; i < userForm.decHandler.Count; i++)
                {
                    decHandler.Add(userForm.decHandler[i]);
                    decFunc.Add(userForm.decFunc[i]);
                }
    
                for (int j = 0; j < userForm.CtrlItems.Count; j++)
                {
                    for (int i = 0; i < userForm.CtrlItems[j].decHandler.Count; i++)
                    {
                        decHandler.Add(userForm.CtrlItems[j].decHandler[i]);
                        decFunc.Add(userForm.CtrlItems[j].decFunc[i]);
                    }
                }
    
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }
        }
        private string Create_SourceCode()
        {
            string source = "";
            try
            {
                List<string> lstSuspend = new List<string>();
                List<string> lstResume = new List<string>();
                string space = "";
    
                if (fileInfo.source_base[0].IndexOf(";") == -1)
                {
                    space = space.PadLeft(8);
                }
                else
                {
                    space = space.PadLeft(4);
                }
    
                source = Create_Code_Instance(source, space);
                source = Create_Code_Suspend_Resume(source, lstSuspend, lstResume, space);
    
                // suspend
                for (int i = 0; i < lstSuspend.Count; i++)
                {
                    source += lstSuspend[i];
                }
    
                source = Create_Code_Property(source, space);
                source = Create_Code_FormProperty(source, space);
                source = Create_Code_FormAddControl(source, space);
    
                // resume
                for (int i = 0; i < lstResume.Count; i++)
                {
                    source += lstResume[i];
                }
    
                source = Create_Code_EventDeclaration(source, space);
    
                if (fileInfo.source_base[0].IndexOf(";") == -1)
                {
                    source += "    }\r\n";
                }
                source += "}\r\n";
                source += "\r\n";
    
                // events function
                source = Create_Code_FuncDeclaration(source);
    
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }
            return source;
        }
        private string Create_Code_FuncDeclaration(string source)
        {
            try
            {
                // control
                for (int i = 0; i < userForm.CtrlItems.Count; i++)
                {
                    for (int j = 0; j < userForm.CtrlItems[i].decFunc.Count; j++)
                    {
                        source += "//" + userForm.CtrlItems[i].decFunc[j] + "\r\n";
                        source += "//{\r\n";
                        source += "//\r\n";
                        source += "//}\r\n";
                        source += "\r\n";
                    }
                }
    
                // form
                for (int i = 0; i < userForm.decFunc.Count; i++)
                {
                    source += "//" + userForm.decFunc[i] + "\r\n";
                    source += "//{\r\n";
                    source += "//\r\n";
                    source += "//}\r\n";
                    source += "\r\n";
                }
    
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }

            return source;
        }
        private string Create_Code_EventDeclaration(string source, string space)
        {
            try
            {
                source += space + "}\r\n";
                source += "\r\n";
                source += space + "#endregion\r\n";
                source += "\r\n";
    
                // declaration
                for (int i = 0; i < userForm.CtrlItems.Count; i++)
                {
                    string[] split = userForm.CtrlItems[i].ctrl.GetType().ToString().Split('.');
                    string dec = split[split.Length - 1];
                    source += space + "private " + dec + " " + userForm.CtrlItems[i].ctrl.Name + ";\r\n";
    
                }
    
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }
            return source;
        }
        private string Create_Code_FormAddControl(string source, string space)
        {
            try
            {
                // AddControl
                for (int i = 0; i < userForm.CtrlItems.Count; i++)
                {
                    if (userForm.CtrlItems[i].ctrl.Parent == userForm)
                    {
                        source += space + "    this.Controls.Add(this." + userForm.CtrlItems[i].ctrl.Name + ");\r\n";
                    }
                }
    
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }
            return source;
        }
        private string Create_Code_FormProperty(string source, string space)
        {
            try
            {
                // form-property
                source += space + "    //\r\n";
                source += space + "    // form\r\n";
                source += space + "    //\r\n";
    
                foreach (PropertyInfo item in userForm.memForm.GetType().GetProperties())
                {
                    if (DesignerControls.HideProperty(item.Name))
                    {
                        Control baseForm = new Form();
                        Control memForm = userForm.memForm as Control;
    
                        if (item.GetValue(memForm) != null && item.GetValue(baseForm) != null)
                        {
                            if (item.GetValue(memForm).ToString() != item.GetValue(baseForm).ToString())
                            {
                                string str1 = space + "    this." + item.Name;
                                string strProperty = DesignerControls.Property2String(memForm, item);
    
                                if (strProperty != "")
                                {
                                    source += str1 + strProperty + "\r\n";
                                }
                            }
                        }
                    }
                }
                source = Create_Code_FormEventsDec(source, space, userForm);
    
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }
            return source;
        }
        private string Create_Code_Property(string source, string space)
        {
            try
            {
                // Property
                for (int i = 0; i < userForm.CtrlItems.Count; i++)
                {
                    string memCode = "";
                    source += space + "    //\r\n";
                    source += space + "    // " + userForm.CtrlItems[i].ctrl.Name + "\r\n";
                    source += space + "    //\r\n";
    
                    source = Create_Code_AddControl(source, space, i);
    
                    // Property
                    foreach (PropertyInfo item in userForm.CtrlItems[i].ctrl.GetType().GetProperties())
                    {
                        if (DesignerControls.HideProperty(item.Name))
                        {
                            Get_Code_Property(ref source, ref memCode, item, userForm.CtrlItems[i], space);
                        }
                    }
                    if (memCode != "")
                    {
                        source += memCode;
                    }
    
                    source = Create_Code_EventsDec(source, space, userForm.CtrlItems[i]);
                }
    
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }
            return source;
        }
        private static void Get_Code_Property(ref string source, ref string memCode, PropertyInfo item, DesignerControls ctrlItems, string space)
        {
            try
            {
                Control baseCtrl = ctrlItems.GetBaseCtrl();
                if (item.GetValue(ctrlItems.ctrl) != null && item.GetValue(baseCtrl) != null)
                {
                    if (item.GetValue(ctrlItems.ctrl).ToString() != item.GetValue(baseCtrl).ToString())
                    {
                        string str1 = space + "    this." + ctrlItems.ctrl.Name + "." + item.Name;
                        string strProperty = DesignerControls.Property2String(ctrlItems.ctrl, item);
                        if (strProperty != "")
                        {
                            if (item.Name != "SplitterDistance" && item.Name != "Anchor")
                            {
                                source += str1 + strProperty + "\r\n";
                            }
                            else
                            {
                                memCode += str1 + strProperty + "\r\n";
                            }
                        }
                    }
                }
    
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }
        }
        private static string Create_Code_EventsDec(string source, string space, DesignerControls cls_ctrl)
        {
            try
            {
                for (int i = 0; i < cls_ctrl.decHandler.Count; i++)
                {
                    source += space + "    " + cls_ctrl.decHandler[i] + "\r\n";
                }
    
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }
            return source;
        }
        private static string Create_Code_FormEventsDec(string source, string space, DesignerCanvasForm userForm)
        {
            try
            {
                for (int i = 0; i < userForm.decHandler.Count; i++)
                {
                    source += space + "    " + userForm.decHandler[i] + "\r\n";
                }
    
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }
            return source;
        }
        private string Create_Code_AddControl(string source, string space, int i)
        {
            try
            {
                // AddControl
                for (int j = 0; j < userForm.CtrlItems.Count; j++)
                {
                    if (userForm.CtrlItems[i].ctrl.Name == userForm.CtrlItems[j].ctrl.Parent.Name)
                    {
                        source += space + "    this." + userForm.CtrlItems[i].ctrl.Name + ".Controls.Add(this." + userForm.CtrlItems[j].ctrl.Name + ");\r\n";
                    }
                    else if (userForm.CtrlItems[i].ctrl.Name == userForm.CtrlItems[j].ctrl.Parent.Parent.Name)
                    {
                        if (userForm.CtrlItems[j].ctrl.Parent.Name.IndexOf("Panel1") > -1)
                        {
                            source += space + "    this." + userForm.CtrlItems[i].ctrl.Name + ".Panel1.Controls.Add(this." + userForm.CtrlItems[j].ctrl.Name + ");\r\n";
                        }
                        else if (userForm.CtrlItems[j].ctrl.Parent.Name.IndexOf("Panel2") > -1)
                        {
                            source += space + "    this." + userForm.CtrlItems[i].ctrl.Name + ".Panel2.Controls.Add(this." + userForm.CtrlItems[j].ctrl.Name + ");\r\n";
                        }
                    }
                }
    
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }
            return source;
        }
        private string Create_Code_Suspend_Resume(string source, List<string> lstSuspend, List<string> lstResume, string space)
        {
            try
            {
                // Suspend & resume
                for (int i = 0; i < userForm.CtrlItems.Count; i++)
                {
                    source += space + "    this." + userForm.CtrlItems[i].ctrl.Name + " = new System.Windows.Forms." + userForm.CtrlItems[i].className + "();\r\n";
    
                    List<string> className_group1 = new List<string>()
                    {
                        "DataGridView",
                        "PictureBox",
                        "SplitContainer"
                    };
                    for (int j = 0; j < className_group1.Count; j++)
                    {
                        if (userForm.CtrlItems[i].className == className_group1[j])
                        {
                            lstSuspend.Add(space + "    ((System.ComponentModel.ISupportInitialize)(this." + userForm.CtrlItems[i].ctrl.Name + ")).BeginInit();\r\n");
                            lstResume.Add(space + "    ((System.ComponentModel.ISupportInitialize)(this." + userForm.CtrlItems[i].ctrl.Name + ")).EndInit();\r\n");
                        }
                    }
    
                    List<string> className_group2 = new List<string>()
                    {
                        "GroupBox",
                        "Panel",
                        "StatusStrip",
                        "TabControl",
                        "TabPage"
                    };
                    for (int j = 0; j < className_group2.Count; j++)
                    {
                        if (userForm.CtrlItems[i].className == className_group2[j])
                        {
                            lstSuspend.Add(space + "    this." + userForm.CtrlItems[i].ctrl.Name + ".SuspendLayout();\r\n");
                            lstResume.Add(space + "    this." + userForm.CtrlItems[i].ctrl.Name + ".ResumeLayout(false);\r\n");
                        }
                    }
    
                    if (userForm.CtrlItems[i].className == "SplitContainer")
                    {
                        lstSuspend.Add(space + "    this." + userForm.CtrlItems[i].ctrl.Name + ".Panel1.SuspendLayout();\r\n");
                        lstSuspend.Add(space + "    this." + userForm.CtrlItems[i].ctrl.Name + ".Panel2.SuspendLayout();\r\n");
                        lstSuspend.Add(space + "    this." + userForm.CtrlItems[i].ctrl.Name + ".SuspendLayout();\r\n");
                        lstResume.Add(space + "    this." + userForm.CtrlItems[i].ctrl.Name + ".Panel1.ResumeLayout(false);\r\n");
                        lstResume.Add(space + "    this." + userForm.CtrlItems[i].ctrl.Name + ".Panel2.ResumeLayout(false);\r\n");
                        lstResume.Add(space + "    this." + userForm.CtrlItems[i].ctrl.Name + ".ResumeLayout(false);\r\n");
                    }
                }
                lstSuspend.Add(space + "    this.SuspendLayout();\r\n");
                lstResume.Add(space + "    this.ResumeLayout(false);\r\n");
    
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }

            return source;
        }
        private string Create_Code_Instance(string source, string space)
        {
            try
            {
                // Instance
                for (int i = 0; i < fileInfo.source_base.Count; i++)
                {
                    source += fileInfo.source_base[i] + "\r\n";
                }
                source += space + "{\r\n";
    
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }

            return source;
        }
        private void Show_ControlViewShow(DesignerCanvasForm form)
        {
            try
            {
                ctrlTreeView.Nodes.Clear();
                TreeNode NodeRoot = new TreeNode("Form");
                DesignerTreeNode[] itemNode = new DesignerTreeNode[] {};
    
                for (int i = 0; i < form.CtrlItems.Count; i++)
                {
                    if (form.CtrlItems[i].ctrl.Parent == form)
                    {
                        Array.Resize(ref itemNode, itemNode.Length + 1);
                        if (form.CtrlItems[i].className == "SplitContainer")
                        {
                            itemNode[itemNode.Length - 1] = new DesignerTreeNode(form.CtrlItems[i].ctrl.Name + ".Panel1");
                            Array.Resize(ref itemNode, itemNode.Length + 1);
                            itemNode[itemNode.Length - 1] = new DesignerTreeNode(form.CtrlItems[i].ctrl.Name + ".Panel2");
                        }
                        else
                        {
                            itemNode[itemNode.Length - 1] = new DesignerTreeNode(form.CtrlItems[i].ctrl.Name);
                        }
                    }
                    else
                    {
                        for (int j = 0; j < itemNode.Length; j++)
                        {
                            DesignerTreeNode retNode = itemNode[j].Search(form.CtrlItems[i].ctrl.Parent.Name);
                            if (retNode != null)
                            {
                                retNode.Add(form.CtrlItems[i].ctrl.Name, form.CtrlItems[i].className);
                                break;
                            }
                        }
                    }
                }
    
                if (itemNode.Length > 0)
                {
                    NodeRoot.Nodes.AddRange(itemNode);
                }
    
                ctrlTreeView.Nodes.Add(NodeRoot);
                ctrlTreeView.TopNode.Expand();
    
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }
        }

        #region Utility
        private void StartProgressBar()
        {
            StatusBarProgressBar.Value = 33;
            StatusBarProgressBar.Style = ProgressBarStyle.Marquee;//true;
            StatusBarProgressBar.Visible = true;
            //DoEvents;
        }

        private void StopProgressBar()
        {
            //DoEvents;
            StatusBarProgressBar.Visible = false;
        }

        private void StartActionIcon(Image resourceItem/*String resourceItemId*/)
        {
            StatusBarActionIcon.Image = (resourceItem == null ? null : resourceItem);//global::MonoApp1.Properties.Resources.New; //"New", etc.
            StatusBarActionIcon.Visible = true;
        }

        private void StopActionIcon()
        {
            StatusBarActionIcon.Visible = false;
            StatusBarActionIcon.Image = global::WinFormsDesigner.CSharp.Mono.Properties.Resources.New; 
        }
        #endregion Utility
    }
}