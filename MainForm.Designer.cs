﻿using System;
using System.Windows.Forms;

namespace WinFormsDesigner.CSharp.Mono
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainWndSplitContainer = new System.Windows.Forms.SplitContainer();
            this.ctrlsTab = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.ctrlLstBox = new System.Windows.Forms.ListBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.ctrlTreeView = new System.Windows.Forms.TreeView();
            this.subWndSplitContainer = new System.Windows.Forms.SplitContainer();
            this.designeTab = new System.Windows.Forms.TabControl();
            this.designePage = new System.Windows.Forms.TabPage();
            this.userForm = new WinFormsDesigner.CSharp.Mono.DesignerCanvasForm();
            this.sourcePage = new System.Windows.Forms.TabPage();
            this.sourceTxtBox = new System.Windows.Forms.TextBox();
            this.eventsPage = new System.Windows.Forms.TabPage();
            this.eventTxtBox = new System.Windows.Forms.TextBox();
            this.logPage = new System.Windows.Forms.TabPage();
            this.logTxtBox = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.nameTxtBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.propertyBox = new System.Windows.Forms.PropertyGrid();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.evtGridView = new WinFormsDesigner.CSharp.Mono.FormEventsDataGridView();
            this.statusBar = new System.Windows.Forms.StatusStrip();
            this.StatusBarStatusMessage = new System.Windows.Forms.ToolStripStatusLabel();
            this.StatusBarErrorMessage = new System.Windows.Forms.ToolStripStatusLabel();
            this.StatusBarCustomMessage = new System.Windows.Forms.ToolStripStatusLabel();
            this.StatusBarProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.StatusBarActionIcon = new System.Windows.Forms.ToolStripStatusLabel();
            this.StatusBarDirtyIcon = new System.Windows.Forms.ToolStripStatusLabel();
            this.StatusBarNetworkIndicator = new System.Windows.Forms.ToolStripStatusLabel();
            this.menu = new System.Windows.Forms.MenuStrip();
            this.menuFile = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFileNew = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFileOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFileSave = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFileClose = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFileSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.menuFileQuit = new System.Windows.Forms.ToolStripMenuItem();
            this.menuEdit = new System.Windows.Forms.ToolStripMenuItem();
            // this.menuEditUndo = new System.Windows.Forms.ToolStripMenuItem();
            // this.menuEditRedo = new System.Windows.Forms.ToolStripMenuItem();
            // this.menuEditSeparator0 = new System.Windows.Forms.ToolStripSeparator();
            this.menuEditSelectAll = new System.Windows.Forms.ToolStripMenuItem();
            this.menuEditCut = new System.Windows.Forms.ToolStripMenuItem();
            this.menuEditCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.menuEditPaste = new System.Windows.Forms.ToolStripMenuItem();
            this.menuEditDelete = new System.Windows.Forms.ToolStripMenuItem();
            // this.menuEditSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            // this.menuEditFind = new System.Windows.Forms.ToolStripMenuItem();
            // this.menuEditSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            // this.menuEditReplace = new System.Windows.Forms.ToolStripMenuItem();
            // this.menuEditRefresh = new System.Windows.Forms.ToolStripMenuItem();
            this.menuEditSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.menuEditPreferences = new System.Windows.Forms.ToolStripMenuItem();
            // this.menuEditProperties = new System.Windows.Forms.ToolStripMenuItem();
            this.menuHelp = new System.Windows.Forms.ToolStripMenuItem();
            // this.menuHelpContents = new System.Windows.Forms.ToolStripMenuItem();
            // this.menuHelpIndex = new System.Windows.Forms.ToolStripMenuItem();
            // this.menuHelpOnlineHelp = new System.Windows.Forms.ToolStripMenuItem();
            // this.menuHelpSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            // this.menuHelpLicenceInformation = new System.Windows.Forms.ToolStripMenuItem();
            // this.menuHelpCheckForUpdates = new System.Windows.Forms.ToolStripMenuItem();
            // this.menuHelpSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.menuHelpAbout = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.mainWndSplitContainer)).BeginInit();
            this.mainWndSplitContainer.Panel1.SuspendLayout();
            this.mainWndSplitContainer.Panel2.SuspendLayout();
            this.mainWndSplitContainer.SuspendLayout();
            this.ctrlsTab.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.subWndSplitContainer)).BeginInit();
            this.subWndSplitContainer.Panel1.SuspendLayout();
            this.subWndSplitContainer.Panel2.SuspendLayout();
            this.subWndSplitContainer.SuspendLayout();
            this.designeTab.SuspendLayout();
            this.designePage.SuspendLayout();
            this.sourcePage.SuspendLayout();
            this.eventsPage.SuspendLayout();
            this.logPage.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.evtGridView)).BeginInit();
            this.menu.SuspendLayout();
            this.statusBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainWndSplitContainer
            // 
            this.mainWndSplitContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mainWndSplitContainer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.mainWndSplitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.mainWndSplitContainer.Location = new System.Drawing.Point(0, 31);
            this.mainWndSplitContainer.Name = "mainWndSplitContainer";
            // 
            // mainWndSplitContainer.Panel1
            // 
            this.mainWndSplitContainer.Panel1.Controls.Add(this.ctrlsTab);
            // 
            // mainWndSplitContainer.Panel2
            // 
            this.mainWndSplitContainer.Panel2.Controls.Add(this.subWndSplitContainer);
            this.mainWndSplitContainer.Size = new System.Drawing.Size(1003, 608);
            this.mainWndSplitContainer.SplitterDistance = 199;
            this.mainWndSplitContainer.TabIndex = 0;
            // 
            // ctrlsTab
            // 
            this.ctrlsTab.Controls.Add(this.tabPage1);
            this.ctrlsTab.Controls.Add(this.tabPage2);
            this.ctrlsTab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctrlsTab.Location = new System.Drawing.Point(0, 0);
            this.ctrlsTab.Name = "ctrlsTab";
            this.ctrlsTab.SelectedIndex = 0;
            this.ctrlsTab.Size = new System.Drawing.Size(195, 604);
            this.ctrlsTab.TabIndex = 0;
            this.ctrlsTab.SelectedIndexChanged += new System.EventHandler(this.ctrlsTab_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.ctrlLstBox);
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(187, 571);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "ToolBox";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // ctrlLstBox
            // 
            this.ctrlLstBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctrlLstBox.FormattingEnabled = true;
            this.ctrlLstBox.ItemHeight = 20;
            this.ctrlLstBox.Location = new System.Drawing.Point(3, 3);
            this.ctrlLstBox.Name = "ctrlLstBox";
            this.ctrlLstBox.Size = new System.Drawing.Size(181, 565);
            this.ctrlLstBox.Sorted = true;
            this.ctrlLstBox.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.ctrlTreeView);
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(187, 571);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "TreeView";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // ctrlTreeView
            // 
            this.ctrlTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctrlTreeView.Location = new System.Drawing.Point(3, 3);
            this.ctrlTreeView.Name = "ctrlTreeView";
            this.ctrlTreeView.Size = new System.Drawing.Size(181, 565);
            this.ctrlTreeView.TabIndex = 0;
            this.ctrlTreeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.ctrlTreeView_AfterSelect);
            // 
            // subWndSplitContainer
            // 
            this.subWndSplitContainer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.subWndSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.subWndSplitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.subWndSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.subWndSplitContainer.Name = "subWndSplitContainer";
            // 
            // subWndSplitContainer.Panel1
            // 
            this.subWndSplitContainer.Panel1.AutoScroll = true;
            this.subWndSplitContainer.Panel1.BackColor = System.Drawing.Color.White;
            this.subWndSplitContainer.Panel1.Controls.Add(this.designeTab);
            // 
            // subWndSplitContainer.Panel2
            // 
            this.subWndSplitContainer.Panel2.Controls.Add(this.tabControl1);
            this.subWndSplitContainer.Size = new System.Drawing.Size(800, 608);
            this.subWndSplitContainer.SplitterDistance = 525;
            this.subWndSplitContainer.TabIndex = 0;
            // 
            // designeTab
            // 
            this.designeTab.Controls.Add(this.designePage);
            this.designeTab.Controls.Add(this.sourcePage);
            this.designeTab.Controls.Add(this.eventsPage);
            this.designeTab.Controls.Add(this.logPage);
            this.designeTab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.designeTab.Location = new System.Drawing.Point(0, 0);
            this.designeTab.Name = "designeTab";
            this.designeTab.SelectedIndex = 0;
            this.designeTab.Size = new System.Drawing.Size(521, 604);
            this.designeTab.TabIndex = 1;
            this.designeTab.SelectedIndexChanged += new System.EventHandler(this.designeTab_SelectedIndexChanged);
            // 
            // designePage
            // 
            this.designePage.AutoScroll = true;
            this.designePage.Controls.Add(this.userForm);
            this.designePage.Location = new System.Drawing.Point(4, 29);
            this.designePage.Name = "designePage";
            this.designePage.Padding = new System.Windows.Forms.Padding(3);
            this.designePage.Size = new System.Drawing.Size(513, 571);
            this.designePage.TabIndex = 0;
            this.designePage.Text = "Design";
            this.designePage.UseVisualStyleBackColor = true;
            // 
            // userForm
            // 
            this.userForm.BackColor = System.Drawing.Color.WhiteSmoke;
            this.userForm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.userForm.Location = new System.Drawing.Point(16, 16);
            this.userForm.Name = "userForm";
            this.userForm.Size = new System.Drawing.Size(480, 400);
            this.userForm.TabIndex = 0;
            // 
            // sourcePage
            // 
            this.sourcePage.Controls.Add(this.sourceTxtBox);
            this.sourcePage.Location = new System.Drawing.Point(4, 29);
            this.sourcePage.Name = "sourcePage";
            this.sourcePage.Padding = new System.Windows.Forms.Padding(3);
            this.sourcePage.Size = new System.Drawing.Size(513, 571);
            this.sourcePage.TabIndex = 1;
            this.sourcePage.Text = "Source";
            this.sourcePage.UseVisualStyleBackColor = true;
            // 
            // sourceTxtBox
            // 
            this.sourceTxtBox.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.sourceTxtBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sourceTxtBox.Location = new System.Drawing.Point(3, 3);
            this.sourceTxtBox.Multiline = true;
            this.sourceTxtBox.Name = "sourceTxtBox";
            this.sourceTxtBox.ReadOnly = true;
            this.sourceTxtBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.sourceTxtBox.Size = new System.Drawing.Size(507, 565);
            this.sourceTxtBox.TabIndex = 0;
            this.sourceTxtBox.WordWrap = false;
            // 
            // eventsPage
            // 
            this.eventsPage.Controls.Add(this.eventTxtBox);
            this.eventsPage.Location = new System.Drawing.Point(4, 29);
            this.eventsPage.Name = "eventsPage";
            this.eventsPage.Padding = new System.Windows.Forms.Padding(3);
            this.eventsPage.Size = new System.Drawing.Size(513, 571);
            this.eventsPage.TabIndex = 2;
            this.eventsPage.Text = "Events";
            this.eventsPage.UseVisualStyleBackColor = true;
            // 
            // eventTxtBox
            // 
            this.eventTxtBox.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.eventTxtBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.eventTxtBox.Location = new System.Drawing.Point(3, 3);
            this.eventTxtBox.Multiline = true;
            this.eventTxtBox.Name = "eventTxtBox";
            this.eventTxtBox.ReadOnly = true;
            this.eventTxtBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.eventTxtBox.Size = new System.Drawing.Size(507, 565);
            this.eventTxtBox.TabIndex = 0;
            this.eventTxtBox.WordWrap = false;
            // 
            // logPage
            // 
            this.logPage.Controls.Add(this.logTxtBox);
            this.logPage.Location = new System.Drawing.Point(4, 29);
            this.logPage.Name = "logPage";
            this.logPage.Padding = new System.Windows.Forms.Padding(3);
            this.logPage.Size = new System.Drawing.Size(513, 571);
            this.logPage.TabIndex = 3;
            this.logPage.Text = "Log";
            this.logPage.UseVisualStyleBackColor = true;
            // 
            // logTxtBox
            // 
            this.logTxtBox.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.logTxtBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.logTxtBox.Location = new System.Drawing.Point(3, 3);
            this.logTxtBox.Multiline = true;
            this.logTxtBox.Name = "logTxtBox";
            this.logTxtBox.ReadOnly = true;
            this.logTxtBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.logTxtBox.Size = new System.Drawing.Size(507, 565);
            this.logTxtBox.TabIndex = 0;
            this.logTxtBox.WordWrap = false;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(267, 604);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.nameTxtBox);
            this.tabPage3.Controls.Add(this.label1);
            this.tabPage3.Controls.Add(this.propertyBox);
            this.tabPage3.Location = new System.Drawing.Point(4, 29);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(259, 571);
            this.tabPage3.TabIndex = 0;
            this.tabPage3.Text = "Property";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // nameTxtBox
            // 
            this.nameTxtBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nameTxtBox.Location = new System.Drawing.Point(58, 6);
            this.nameTxtBox.Name = "nameTxtBox";
            this.nameTxtBox.Size = new System.Drawing.Size(195, 27);
            this.nameTxtBox.TabIndex = 2;
            this.nameTxtBox.TextChanged += new System.EventHandler(this.nameTxtBox_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(0, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Name";
            // 
            // propertyBox
            // 
            this.propertyBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.propertyBox.Location = new System.Drawing.Point(3, 39);
            this.propertyBox.Name = "propertyBox";
            this.propertyBox.Size = new System.Drawing.Size(253, 526);
            this.propertyBox.TabIndex = 0;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.evtGridView);
            this.tabPage4.Location = new System.Drawing.Point(4, 29);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(259, 567);
            this.tabPage4.TabIndex = 1;
            this.tabPage4.Text = "Events";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // evtGridView
            // 
            this.evtGridView.AllowUserToAddRows = false;
            this.evtGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.evtGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.evtGridView.Location = new System.Drawing.Point(3, 3);
            this.evtGridView.Name = "evtGridView";
            this.evtGridView.RowHeadersWidth = 51;
            this.evtGridView.RowTemplate.Height = 29;
            this.evtGridView.Size = new System.Drawing.Size(253, 561);
            this.evtGridView.TabIndex = 0;
            // 
            // statusBar
            // 
            this.statusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusBarStatusMessage,
            this.StatusBarErrorMessage,
            this.StatusBarProgressBar,
            this.StatusBarActionIcon,
            this.StatusBarDirtyIcon,
            this.StatusBarNetworkIndicator});
            this.statusBar.Dock = System.Windows.Forms.DockStyle.Bottom;
            // this.statusBar.ImageScalingSize = new System.Drawing.Size(20, 20);
            //this.statusBar.Location = new System.Drawing.Point(0, 642);
            this.statusBar.Name = "statusBar";
            this.statusBar.Size = new System.Drawing.Size(1003, 22);
            this.statusBar.TabIndex = 1;
            this.statusBar.Text = "statusBar";
            // 
            // StatusBarStatusMessage
            // 
            this.StatusBarStatusMessage.ForeColor = System.Drawing.Color.Green;
            this.StatusBarStatusMessage.Name = "StatusBarStatusMessage";
            this.StatusBarStatusMessage.Size = new System.Drawing.Size(0, 17);
            this.StatusBarStatusMessage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.StatusBarStatusMessage.Text = "";
            // 
            // StatusBarErrorMessage
            // 
            this.StatusBarErrorMessage.AutoToolTip = true;
            this.StatusBarErrorMessage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.StatusBarErrorMessage.ForeColor = System.Drawing.Color.Red;
            this.StatusBarErrorMessage.Name = "StatusBarErrorMessage";
            this.StatusBarErrorMessage.Size = new System.Drawing.Size(609, 17);
            this.StatusBarErrorMessage.Spring = true;
            this.StatusBarErrorMessage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.StatusBarErrorMessage.Text = "";
            // 
            // StatusBarCustomMessage
            // 
            this.StatusBarCustomMessage.Name = "StatusBarCustomMessage";
            this.StatusBarCustomMessage.Size = new System.Drawing.Size(0, 17);
            // 
            // StatusBarProgressBar
            // 
            this.StatusBarProgressBar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.StatusBarProgressBar.Name = "StatusBarProgressBar";
            this.StatusBarProgressBar.Size = new System.Drawing.Size(100, 16);
            this.StatusBarProgressBar.Value = 10;
            this.StatusBarProgressBar.Visible = false;
            // 
            // StatusBarActionIcon
            // 
            this.StatusBarActionIcon.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.StatusBarActionIcon.Image = global::WinFormsDesigner.CSharp.Mono.Properties.Resources.New;
            this.StatusBarActionIcon.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.StatusBarActionIcon.Name = "StatusBarActionIcon";
            this.StatusBarActionIcon.Size = new System.Drawing.Size(16, 17);
            this.StatusBarActionIcon.Visible = false;
            // 
            // StatusBarDirtyMessage
            // 
            this.StatusBarDirtyIcon.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.StatusBarDirtyIcon.Image = global::WinFormsDesigner.CSharp.Mono.Properties.Resources.Save;
            this.StatusBarDirtyIcon.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.StatusBarDirtyIcon.Name = "StatusBarDirtyMessage";
            this.StatusBarDirtyIcon.Size = new System.Drawing.Size(16, 17);
            this.StatusBarDirtyIcon.ToolTipText = "Dirty";
            this.StatusBarDirtyIcon.Visible = false;
            // 
            // StatusBarNetworkIndicator
            // 
            this.StatusBarNetworkIndicator.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.StatusBarNetworkIndicator.Image = global::WinFormsDesigner.CSharp.Mono.Properties.Resources.Network;
            this.StatusBarNetworkIndicator.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.StatusBarNetworkIndicator.ImageTransparentColor = System.Drawing.Color.Fuchsia;
            this.StatusBarNetworkIndicator.Name = "StatusBarNetworkIndicator";
            this.StatusBarNetworkIndicator.Size = new System.Drawing.Size(16, 17);
            this.StatusBarNetworkIndicator.ToolTipText = "Network Connected";
            this.StatusBarNetworkIndicator.Visible = false;
            // 
            // menu
            // 
            this.menu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuFile,
            this.menuEdit,
            this.menuHelp});
            this.menu.Location = new System.Drawing.Point(0, 0);
            this.menu.Name = "menu";
            this.menu.Size = new System.Drawing.Size(1003, 28);
            this.menu.TabIndex = 2;
            this.menu.Text = "menu";
            // 
            // menuFile
            // 
            this.menuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuFileNew,
            this.menuFileOpen,
            this.menuFileSave,
            this.menuFileClose,
            this.menuFileSeparator2,
            this.menuFileQuit
            });
            this.menuFile.Name = "menuFile";
            this.menuFile.Size = new System.Drawing.Size(46, 24);
            this.menuFile.Text = "&File";
            // 
            // menuFileNew
            // 
            this.menuFileNew.Image = global::WinFormsDesigner.CSharp.Mono.Properties.Resources.New;
            this.menuFileNew.Name = "menuFileNew";
            this.menuFileNew.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.menuFileNew.Size = new System.Drawing.Size(128, 26);
            this.menuFileNew.Text = "&New";
            this.menuFileNew.Click += new System.EventHandler(this.menuFileNew_Click);
            // 
            // menuFileOpen
            // 
            this.menuFileOpen.Image = global::WinFormsDesigner.CSharp.Mono.Properties.Resources.Open;
            this.menuFileOpen.Name = "menuFileOpen";
            this.menuFileOpen.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.menuFileOpen.Size = new System.Drawing.Size(128, 26);
            this.menuFileOpen.Text = "Open";
            this.menuFileOpen.Click += new System.EventHandler(this.menuFileOpen_Click);
            // 
            // menuFileSave
            // 
            this.menuFileSave.Image = global::WinFormsDesigner.CSharp.Mono.Properties.Resources.Save;
            this.menuFileSave.Name = "menuFileSave";
            this.menuFileSave.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.menuFileSave.Size = new System.Drawing.Size(128, 26);
            this.menuFileSave.Text = "&Save";
            this.menuFileSave.Click += new System.EventHandler(this.menuFileSave_Click);
            // 
            // menuFileClose
            // 
            this.menuFileClose.Name = "menuFileClose";
            this.menuFileClose.Size = new System.Drawing.Size(128, 26);
            this.menuFileClose.Text = "&Close";
            this.menuFileClose.Click += new System.EventHandler(this.menuFileClose_Click);
            // 
            // menuFileSeparator2
            // 
            this.menuFileSeparator2.Name = "menuFileSeparator2";
            this.menuFileSeparator2.Size = new System.Drawing.Size(146, 6);
            // 
            // menuFileQuit
            // 
            this.menuFileQuit.Name = "menuFileQuit";
            this.menuFileQuit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.menuFileQuit.Size = new System.Drawing.Size(128, 26);
            this.menuFileQuit.Text = "&Quit";
            this.menuFileQuit.Click += new System.EventHandler(this.menuFileQuit_Click);
            // 
            // menuEdit
            // 
            this.menuEdit.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            // this.menuEditUndo,
            // this.menuEditRedo,
            // this.menuEditSeparator0,
            this.menuEditSelectAll,
            this.menuEditCut,
            this.menuEditCopy,
            this.menuEditPaste,
            this.menuEditDelete,
            // this.menuEditSeparator1,
            // this.menuEditFind,
            // this.menuEditReplace,
            // this.menuEditSeparator2,
            // this.menuEditRefresh,
            this.menuEditSeparator3,
            this.menuEditPreferences//,
            // this.menuEditProperties
            });
            this.menuEdit.Name = "menuEdit";
            this.menuEdit.Size = new System.Drawing.Size(49, 24);
            this.menuEdit.Text = "&Edit";
            // // 
            // // menuEditUndo
            // // 
            // this.menuEditUndo.Image = global::WinFormsDesigner.CSharp.Mono.Properties.Resources.Undo;
            // this.menuEditUndo.Name = "menuEditUndo";
            // this.menuEditUndo.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
            // this.menuEditUndo.Size = new System.Drawing.Size(167, 22);
            // this.menuEditUndo.Text = "&Undo";
            // this.menuEditUndo.Click += new System.EventHandler(this.MenuEditUndo_Click);
            // // 
            // // menuEditRedo
            // // 
            // this.menuEditRedo.Image = global::WinFormsDesigner.CSharp.Mono.Properties.Resources.Redo;
            // this.menuEditRedo.Name = "menuEditRedo";
            // this.menuEditRedo.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Y)));
            // this.menuEditRedo.Size = new System.Drawing.Size(167, 22);
            // this.menuEditRedo.Text = "&Redo";
            // this.menuEditRedo.Click += new System.EventHandler(this.MenuEditRedo_Click);
            // // 
            // // menuEditSeparator0
            // // 
            // this.menuEditSeparator0.Name = "menuEditSeparator0";
            // this.menuEditSeparator0.Size = new System.Drawing.Size(164, 6);
            // 
            // menuEditSelectAll
            // 
            this.menuEditSelectAll.Name = "menuEditSelectAll";
            this.menuEditSelectAll.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.menuEditSelectAll.Size = new System.Drawing.Size(167, 22);
            this.menuEditSelectAll.Text = "Select &All";
            this.menuEditSelectAll.Click += new System.EventHandler(this.MenuEditSelectAll_Click);
            // 
            // menuEditCut
            // 
            this.menuEditCut.Image = global::WinFormsDesigner.CSharp.Mono.Properties.Resources.Cut;
            this.menuEditCut.Name = "menuEditCut";
            this.menuEditCut.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.menuEditCut.Size = new System.Drawing.Size(167, 22);
            this.menuEditCut.Text = "Cu&t";
            this.menuEditCut.Click += new System.EventHandler(this.MenuEditCut_Click);
            // 
            // menuEditCopy
            // 
            this.menuEditCopy.Image = global::WinFormsDesigner.CSharp.Mono.Properties.Resources.Copy;
            this.menuEditCopy.ImageTransparentColor = System.Drawing.Color.Black;
            this.menuEditCopy.Name = "menuEditCopy";
            this.menuEditCopy.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.menuEditCopy.Size = new System.Drawing.Size(167, 22);
            this.menuEditCopy.Text = "&Copy";
            this.menuEditCopy.Click += new System.EventHandler(this.MenuEditCopy_Click);
            // 
            // menuEditPaste
            // 
            this.menuEditPaste.Image = global::WinFormsDesigner.CSharp.Mono.Properties.Resources.Paste;
            this.menuEditPaste.Name = "menuEditPaste";
            this.menuEditPaste.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.menuEditPaste.Size = new System.Drawing.Size(167, 22);
            this.menuEditPaste.Text = "&Paste";
            this.menuEditPaste.Click += new System.EventHandler(this.MenuEditPaste_Click);
            // 
            // menuEditDelete
            // 
            this.menuEditDelete.Image = global::WinFormsDesigner.CSharp.Mono.Properties.Resources.Delete;
            this.menuEditDelete.Name = "menuEditDelete";
            this.menuEditDelete.ShortcutKeys = System.Windows.Forms.Keys.Delete;
            this.menuEditDelete.Size = new System.Drawing.Size(167, 22);
            this.menuEditDelete.Text = "&Delete";
            this.menuEditDelete.Click += new System.EventHandler(this.MenuEditDelete_Click);
            // // 
            // // menuEditSeparator1
            // // 
            // this.menuEditSeparator1.Name = "menuEditSeparator1";
            // this.menuEditSeparator1.Size = new System.Drawing.Size(164, 6);
            // // 
            // // menuEditFind
            // // 
            // this.menuEditFind.Image = global::WinFormsDesigner.CSharp.Mono.Properties.Resources.Find;
            // this.menuEditFind.Name = "menuEditFind";
            // this.menuEditFind.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            // this.menuEditFind.Size = new System.Drawing.Size(167, 22);
            // this.menuEditFind.Text = "&Find...";
            // this.menuEditFind.Click += new System.EventHandler(this.MenuEditFind_Click);
            // // 
            // // menuEditReplace
            // // 
            // this.menuEditReplace.Image = global::WinFormsDesigner.CSharp.Mono.Properties.Resources.Replace;
            // this.menuEditReplace.Name = "menuEditReplace";
            // this.menuEditReplace.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.H)));
            // this.menuEditReplace.Size = new System.Drawing.Size(167, 22);
            // this.menuEditReplace.Text = "Rep&lace...";
            // this.menuEditReplace.Click += new System.EventHandler(this.MenuEditReplace_Click);
            // // 
            // // menuEditSeparator2
            // // 
            // this.menuEditSeparator2.Name = "menuEditSeparator2";
            // this.menuEditSeparator2.Size = new System.Drawing.Size(164, 6);
            // // 
            // // menuEditRefresh
            // // 
            // this.menuEditRefresh.Image = global::WinFormsDesigner.CSharp.Mono.Properties.Resources.Reload;
            // this.menuEditRefresh.Name = "menuEditRefresh";
            // this.menuEditRefresh.ShortcutKeys = System.Windows.Forms.Keys.F5;
            // this.menuEditRefresh.Size = new System.Drawing.Size(167, 22);
            // this.menuEditRefresh.Text = "R&efresh";
            // this.menuEditRefresh.Click += new System.EventHandler(this.MenuEditRefresh_Click);
            // 
            // menuEditSeparator3
            // 
            this.menuEditSeparator3.Name = "menuEditSeparator3";
            this.menuEditSeparator3.Size = new System.Drawing.Size(164, 6);
            // 
            // menuEditPreferences
            // 
            this.menuEditPreferences.Image = global::WinFormsDesigner.CSharp.Mono.Properties.Resources.Preferences;
            this.menuEditPreferences.Name = "menuEditPreferences";
            this.menuEditPreferences.Size = new System.Drawing.Size(167, 22);
            this.menuEditPreferences.Text = "Prefere&nces...";
            this.menuEditPreferences.Click += new System.EventHandler(this.MenuEditPreferences_Click);
            // // 
            // // menuEditProperties
            // // 
            // this.menuEditProperties.Image = global::WinFormsDesigner.CSharp.Mono.Properties.Resources.Properties;
            // this.menuEditProperties.ImageTransparentColor = System.Drawing.Color.Black;
            // this.menuEditProperties.Name = "menuEditProperties";
            // this.menuEditProperties.Size = new System.Drawing.Size(167, 22);
            // this.menuEditProperties.Text = "Pr&operties...";
            // this.menuEditProperties.Click += new System.EventHandler(this.MenuEditProperties_Click);
            // 
            // menuHelp
            // 
            this.menuHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            // this.menuHelpContents,
            // this.menuHelpIndex,
            // this.menuHelpOnlineHelp,
            // this.menuHelpSeparator2,
            // this.menuHelpLicenceInformation,
            // this.menuHelpCheckForUpdates,
            // this.menuHelpSeparator1,
            this.menuHelpAbout});
            this.menuHelp.Name = "menuHelp";
            this.menuHelp.Size = new System.Drawing.Size(44, 20);
            this.menuHelp.Text = "&Help";
            this.menuHelp.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // // 
            // // menuHelpContents
            // // 
            // this.menuHelpContents.Image = global::WinFormsDesigner.CSharp.Mono.Properties.Resources.Contents;
            // this.menuHelpContents.Name = "menuHelpContents";
            // this.menuHelpContents.ShortcutKeys = System.Windows.Forms.Keys.F1;
            // this.menuHelpContents.Size = new System.Drawing.Size(181, 22);
            // this.menuHelpContents.Text = "&Contents";
            // this.menuHelpContents.Click += new System.EventHandler(this.MenuHelpContents_Click);
            // // 
            // // menuHelpIndex
            // // 
            // this.menuHelpIndex.Name = "menuHelpIndex";
            // this.menuHelpIndex.Size = new System.Drawing.Size(181, 22);
            // this.menuHelpIndex.Text = "&Index";
            // this.menuHelpIndex.Click += new System.EventHandler(this.MenuHelpIndex_Click);
            // // 
            // // menuHelpOnlineHelp
            // // 
            // this.menuHelpOnlineHelp.Name = "menuHelpOnlineHelp";
            // this.menuHelpOnlineHelp.Size = new System.Drawing.Size(181, 22);
            // this.menuHelpOnlineHelp.Text = "&Online Help";
            // this.menuHelpOnlineHelp.Click += new System.EventHandler(this.MenuHelpOnlineHelp_Click);
            // // 
            // // menuHelpSeparator2
            // // 
            // this.menuHelpSeparator2.Name = "menuHelpSeparator2";
            // this.menuHelpSeparator2.Size = new System.Drawing.Size(178, 6);
            // // 
            // // menuHelpLicenceInformation
            // // 
            // this.menuHelpLicenceInformation.Name = "menuHelpLicenceInformation";
            // this.menuHelpLicenceInformation.Size = new System.Drawing.Size(181, 22);
            // this.menuHelpLicenceInformation.Text = "&Licence Information";
            // this.menuHelpLicenceInformation.Click += new System.EventHandler(this.MenuHelpLicenceInformation_Click);
            // // 
            // // menuHelpCheckForUpdates
            // // 
            // this.menuHelpCheckForUpdates.Name = "menuHelpCheckForUpdates";
            // this.menuHelpCheckForUpdates.Size = new System.Drawing.Size(181, 22);
            // this.menuHelpCheckForUpdates.Text = "Check for &Updates";
            // this.menuHelpCheckForUpdates.Click += new System.EventHandler(this.MenuHelpCheckForUpdates_Click);
            // // 
            // // menuHelpSeparator1
            // // 
            // this.menuHelpSeparator1.Name = "menuHelpSeparator1";
            // this.menuHelpSeparator1.Size = new System.Drawing.Size(178, 6);
            // 
            // menuHelpAbout
            // 
            this.menuHelpAbout.Image = global::WinFormsDesigner.CSharp.Mono.Properties.Resources.About;
            this.menuHelpAbout.Name = "menuHelpAbout";
            this.menuHelpAbout.Size = new System.Drawing.Size(181, 22);
            this.menuHelpAbout.Text = "&About WinFormsDesigner.CSharp.Mono ...";
            this.menuHelpAbout.Click += new System.EventHandler(this.MenuHelpAbout_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1003, 664);
            this.Controls.Add(this.statusBar);
            this.Controls.Add(this.menu);
            this.Controls.Add(this.mainWndSplitContainer);
            this.KeyPreview = true;
            this.MainMenuStrip = this.menu;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "WinFormsDesigner.CSharp.Mono (Alpha)";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyDown);
            this.mainWndSplitContainer.Panel1.ResumeLayout(false);
            this.mainWndSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mainWndSplitContainer)).EndInit();
            this.mainWndSplitContainer.ResumeLayout(false);
            this.ctrlsTab.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.subWndSplitContainer.Panel1.ResumeLayout(false);
            this.subWndSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.subWndSplitContainer)).EndInit();
            this.subWndSplitContainer.ResumeLayout(false);
            this.designeTab.ResumeLayout(false);
            this.designePage.ResumeLayout(false);
            this.sourcePage.ResumeLayout(false);
            this.sourcePage.PerformLayout();
            this.eventsPage.ResumeLayout(false);
            this.eventsPage.PerformLayout();
            this.logPage.ResumeLayout(false);
            this.logPage.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.evtGridView)).EndInit();
            this.statusBar.ResumeLayout(false);
            this.statusBar.PerformLayout();
            this.menu.ResumeLayout(false);
            this.menu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer mainWndSplitContainer;
        private System.Windows.Forms.TabControl ctrlsTab;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.SplitContainer subWndSplitContainer;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TextBox nameTxtBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PropertyGrid propertyBox;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.StatusStrip statusBar;
        private System.Windows.Forms.ToolStripStatusLabel StatusBarStatusMessage;
        private System.Windows.Forms.ToolStripStatusLabel StatusBarErrorMessage;
        private System.Windows.Forms.ToolStripStatusLabel StatusBarCustomMessage;
        private System.Windows.Forms.ToolStripProgressBar StatusBarProgressBar;
        private System.Windows.Forms.ToolStripStatusLabel StatusBarActionIcon;
        private System.Windows.Forms.ToolStripStatusLabel StatusBarDirtyIcon;
        private System.Windows.Forms.ToolStripStatusLabel StatusBarNetworkIndicator;
        private System.Windows.Forms.ListBox ctrlLstBox;
        private System.Windows.Forms.TreeView ctrlTreeView;
        private System.Windows.Forms.MenuStrip menu;
        private System.Windows.Forms.ToolStripMenuItem menuFile;
        private System.Windows.Forms.ToolStripMenuItem menuFileNew;
        private System.Windows.Forms.ToolStripMenuItem menuFileOpen;
        private System.Windows.Forms.ToolStripMenuItem menuFileSave;
        private System.Windows.Forms.ToolStripMenuItem menuFileClose;
        private System.Windows.Forms.ToolStripSeparator menuFileSeparator2;
        private System.Windows.Forms.ToolStripMenuItem menuFileQuit;
        private System.Windows.Forms.ToolStripMenuItem menuEdit;
        // private System.Windows.Forms.ToolStripMenuItem menuEditUndo;
        // private System.Windows.Forms.ToolStripMenuItem menuEditRedo;
        // private System.Windows.Forms.ToolStripSeparator menuEditSeparator0;
        private System.Windows.Forms.ToolStripMenuItem menuEditSelectAll;
        private System.Windows.Forms.ToolStripMenuItem menuEditCut;
        private System.Windows.Forms.ToolStripMenuItem menuEditCopy;
        private System.Windows.Forms.ToolStripMenuItem menuEditPaste;
        private System.Windows.Forms.ToolStripMenuItem menuEditDelete;
        // private System.Windows.Forms.ToolStripSeparator menuEditSeparator1;
        // private System.Windows.Forms.ToolStripMenuItem menuEditFind;
        private System.Windows.Forms.ToolStripSeparator menuEditSeparator2;
        // private System.Windows.Forms.ToolStripMenuItem menuEditReplace;
        // private System.Windows.Forms.ToolStripMenuItem menuEditRefresh;
        private System.Windows.Forms.ToolStripSeparator menuEditSeparator3;
        private System.Windows.Forms.ToolStripMenuItem menuEditPreferences;
        // private System.Windows.Forms.ToolStripMenuItem menuEditProperties;
        private System.Windows.Forms.ToolStripMenuItem menuHelp;
        // private System.Windows.Forms.ToolStripMenuItem menuHelpContents;
        // private System.Windows.Forms.ToolStripMenuItem menuHelpIndex;
        // private System.Windows.Forms.ToolStripMenuItem menuHelpOnlineHelp;
        // private System.Windows.Forms.ToolStripSeparator menuHelpSeparator1;
        // private System.Windows.Forms.ToolStripMenuItem menuHelpLicenceInformation;
        // private System.Windows.Forms.ToolStripMenuItem menuHelpCheckForUpdates;
        // private System.Windows.Forms.ToolStripSeparator menuHelpSeparator2;
        private System.Windows.Forms.ToolStripMenuItem menuHelpAbout;
        private System.Windows.Forms.TabControl designeTab;
        private System.Windows.Forms.TabPage designePage;
        private System.Windows.Forms.TabPage sourcePage;
        private System.Windows.Forms.TabPage eventsPage;
        private WinFormsDesigner.CSharp.Mono.DesignerCanvasForm userForm;
        private System.Windows.Forms.TabPage logPage;
        private System.Windows.Forms.TextBox logTxtBox;
        private System.Windows.Forms.TextBox sourceTxtBox;
        private WinFormsDesigner.CSharp.Mono.FormEventsDataGridView evtGridView;
        private System.Windows.Forms.TextBox eventTxtBox;
    }
}