# readme.md - README for WinFormsDesigner.CSharp.Mono v0.1

## About

Forked from 'Simple WinForms Designer For CSharp' at <https://github.com/hry2566/SWD4CS> and ported to Mono. Had to roll back several newer-style C# coding conventions:
~Converted from SDK-style .Net project to Mono project, for the WinForms support.
~replace new() w/ new <objectname()>
~replace case/when w/ if/else
~remove ! null-forgiving operator
~disable nullable
~disable implicit usings
~change split-separator from string to char[] in all Split calls
~translate some text to English
~replace Array.Empty w/ "new arraytype[] {}"
~replace struct containing field-initializers w/ class

![WinFormsDesigner.CSharp.Mono.png](./WinFormsDesigner.CSharp.Mono.png?raw=true "Screenshot")

### VSCode

To nest files associated with forms, use the setting 'Features/Explorer/File Nesting: Patterns':
Item="*.cs", Value="${basename}.resx,${basename}.Designer.cs"

### Instructions for downloading/installing Mono

Locate / install 'mono-complete' from your distribution's package manager.

If you are using Visual Basic, then you will likely also want to install ​mono-vbnc​ for dependencies such as ​Microsoft.VisualBasic​.
sudo apt-get install mono-vbnc

### Build / Run from terminal

Build from the same folder as the .csproj, with "xbuild WinFormsDesigner.CSharp.Mono.csproj".
If you try to run ('mono WinFormsDesigner.CSharp.Mono.exe') from WinFormsDesigner.CSharp.Mono project folder, you will get an error "Cannot open assembly 'WinFormsDesigner.CSharp.Mono.exe': No such file or directory.". Make sure you run from the bin/Debug sub-folder.

### Issues (Mono Winforms)

~When laying out statusbar, menubar and toolbar, docking of the latter two at the top of the form will be governed by the order in which they are added to the form's DesignerControls collection. Note that the most recently added will get first shot at the top of the form, so add the menu after the toolbar.

### Issues (App)

~Renaming Form1 to Form2 in Property tab was not reflected in File|Save, filename was saved OK  when changed by me in file dialog.
~If we're going to bother having a Source view, it needs to be updated with design changes before viewing. This means that there should be a method for going from source to design-data (Open, source edits) and from design-data to source (design edits, Save). This goes back to the need for a unified model with property changes, where design-data and source are properties. However, displaying the dependent one after changes to the edited one should probably be delayed until the dependent one is re-displayed (tab re-activated). PropertyChanged could set flags indicating that their associated view has a delayed refresh waiting to happen.

### History

0.1:
~initial release (Alpha); re-version from 1.0 to 0.1
~'Designe' tab renamed to 'Design'.
~'ToolsBox' tab renamed to 'ToolBox'.
~'log' tab renamed to 'Log'.
~Implemented simple File|New (code only, no menu) for instantiating FILEINFO on startup and preventing error when selecting Source tab.
-Removed Close() call in Save logic.
~Save dialog showed phantom entries in folder that correspond to existing files, but with "(1)" on the end. Was caused by irregular Filter format: "Designer.cs file (*.Designer.cs;*.Designer.cs)|*.Designer.cs;*.Designer.cs". Removed duplication; no idea what original author intended. Perhaps it was supposed to be "*.cs"?
~"could not set X locale modifier" precedes writing of INI and resize issue

Steve Sepan
ssepanus@yahoo.com
6/18/2022
