using System;
using System.IO;
using System.Collections.Generic;
using System.Reflection;
using System.Windows.Forms;

namespace WinFormsDesigner.CSharp.Mono
{
    public class DesignFileInfo
    {
        public string source_FileName = "";
        public List<string> source_base = new List<string>();
        public string filePath = "";
        public List<DesignControlInfo> ctrlInfo = new List<DesignControlInfo>();

        public DesignFileInfo() 
        { 
            this.source_FileName = DesignerFile.FILE_NEW + "." + DesignerFile.FILE_EXT;//"(new).Designer.cs";
            this.source_base = DesignerFile.BlankFile();
        }
    }
}
