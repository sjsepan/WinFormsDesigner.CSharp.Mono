﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Windows.Forms;

namespace WinFormsDesigner.CSharp.Mono
{
    public partial class FormEventsDataGridView : DataGridView
    {
        private DesignerCanvasForm form;
        private DesignerControls cls_ctrl;
        public FormEventsDataGridView()
        {
            try
            {
                InitializeComponent();
                this.DoubleBuffered = true;
                this.AllowUserToAddRows = false;
                this.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(FormEventsDataGridView1_CellMouseDoubleClick);
    
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }
        }

        internal void ShowEventList(bool flag, DesignerCanvasForm form)
        {
            try
            {
                this.form = form;
                this.cls_ctrl = null;
    
                List<string> evnt =  new List<string>();
                List<string> fnc = new List<string>();
                string[] split;
                for (int i = 0; i < form.decHandler.Count; i++)
                {
                    split = form.decHandler[i].Split("+=".ToCharArray())[0].Split('.');
                    evnt.Add(split[split.Length - 1].Trim());
    
                    split = form.decFunc[i].Split('(')[0].Split(' ');
                    fnc.Add(split[split.Length - 1].Trim());
                }
    
                SetEventsData(flag, form.memForm, evnt, fnc);
    
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }
        }
        internal void ShowEventList(bool flag, DesignerControls ctrl)
        {
            try
            {
                this.form = null;
                this.cls_ctrl = ctrl;
    
                List<string> evnt = new List<string>();
                List<string> fnc = new List<string>();
                string[] split;
                for (int i = 0; i < ctrl.decHandler.Count; i++)
                {
                    split = ctrl.decHandler[i].Split("+=".ToCharArray())[0].Split('.');
                    evnt.Add(split[split.Length - 1].Trim());
    
                    split = ctrl.decFunc[i].Split('(')[0].Split(' ');
                    fnc.Add(split[split.Length - 1].Trim());
                }
                SetEventsData(flag, ctrl.ctrl, evnt, fnc);
    
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }
        }

        private void SetEventsData(bool flag, Control ctrl, List<string> evnt, List<string> fnc)
        {
            try
            {
                DataTable table = new DataTable();
    
                table.Columns.Add("Event");
                table.Columns.Add("Function");
    
                if (flag)
                {
                    Type type = ctrl.GetType();
                    MemberInfo[] members = type.GetMembers();
                    foreach (MemberInfo m in members)
                    {
                        string funcName = "";
                        if (m.MemberType.ToString() == "Event")
                        {
                            for (int i = 0; i < evnt.Count; i++)
                            {
                                if (evnt[i] == m.Name)
                                {
                                    funcName = fnc[i];
                                    break;
                                }
                            }
                            table.Rows.Add(m.Name, funcName);
                        }
                    }
                }
    
                this.DataSource = table;
                this.Sort(this.Columns[0], System.ComponentModel.ListSortDirection.Ascending);
                this.Columns[0].ReadOnly = true;
                this.Columns[1].ReadOnly = true;
                this.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                this.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
    
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }
        }
        private void FormEventsDataGridView1_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                Control ctrl;
    
                if (this.form != null)
                {
                    ctrl = this.form.memForm;
                }
                else
                {
                    ctrl = this.cls_ctrl.ctrl;
                }
    
                string eventName = this.Rows[e.RowIndex].Cells[0].Value.ToString();
                string newHandler;
                string funcName = ctrl.Name + "_" + eventName;
                string funcParam = "";
                string param = "";
    
                if (this.Rows[e.RowIndex].Cells[1].Value.ToString() == "")
                {
                    this.Rows[e.RowIndex].Cells[1].Value = funcName;
    
                    Type delegateType = ctrl.GetType().GetEvent(eventName).EventHandlerType;
                    MethodInfo invoke = delegateType.GetMethod("Invoke");
                    ParameterInfo[] pars = invoke.GetParameters();
                    string[] split = delegateType.AssemblyQualifiedName.Split(',');
                    newHandler = "new " + split[0];
    
                    SetArguments(ref funcParam, ref param, pars);
    
                    string decHandler = GetDecHandler(eventName, newHandler, funcName, ctrl.Name);
                    string decFunc = "private void " + funcName + "(" + funcParam + ")";
                    DeclarationAdd(decHandler, decFunc);
                }
                else
                {
                    Delete_Event(e, funcName);
                }
    
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }
        }

        private void Delete_Event(DataGridViewCellMouseEventArgs e, string funcName)
        {
            try
            {
                this.Rows[e.RowIndex].Cells[1].Value = "";
    
                if (this.form != null)
                {
                    for (int i = 0; i < this.form.decFunc.Count; i++)
                    {
                        string[] split = this.form.decFunc[i].Split('(')[0].Split(' ');
    
                        if (split[split.Length - 1] == funcName)
                        {
                            this.form.decHandler.Remove(this.form.decHandler[i]);
                            this.form.decFunc.Remove(this.form.decFunc[i]);
                            break;
                        }
                    }
                }
                else if (this.cls_ctrl != null)
                {
                    for (int i = 0; i < this.cls_ctrl.decFunc.Count; i++)
                    {
                        string[] split = this.cls_ctrl.decFunc[i].Split('(')[0].Split(' ');
    
                        if (split[split.Length - 1] == funcName)
                        {
                            this.cls_ctrl.decHandler.Remove(this.cls_ctrl.decHandler[i]);
                            this.cls_ctrl.decFunc.Remove(this.cls_ctrl.decFunc[i]);
                            break;
                        }
                    }
                }
    
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }
        }

        private void DeclarationAdd(string decHandler, string decFunc)
        {
            try
            {
                if (this.form != null)
                {
                    this.form.decHandler.Add(decHandler);
                    this.form.decFunc.Add(decFunc);
                }
                else
                {
                    this.cls_ctrl.decHandler.Add(decHandler);
                    this.cls_ctrl.decFunc.Add(decFunc);
                }
    
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }
        }

        private string GetDecHandler(string eventName, string newHandler, string funcName, string ctrlName)
        {
            string decHandler = null;
            try
            {
                if (this.form != null)
                {
                    decHandler = "this." + eventName + " += " + newHandler + "(" + funcName + ");";
                }
                else
                {
                    decHandler = "this." + ctrlName + "." + eventName + " += " + newHandler + "(" + funcName + ");";
                }
    
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }
            return decHandler;
        }

        private void SetArguments(ref string funcParam, ref string param, ParameterInfo[] pars)
        {
            try
            {
                foreach (ParameterInfo p in pars)
                {
                    param = p.ParameterType.ToString();
    
                    if (param == "System.Object")
                    {
                        param += "? sender";//TODO:remove nullable here too?
                    }
                    else
                    {
                        param += " e";
                    }
    
                    if (funcParam == "")
                    {
                        funcParam = param;
                    }
                    else
                    {
                        funcParam += ", " + param;
                    }
                }
    
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(MethodBase.GetCurrentMethod() + ":" + ex.Message);
            }
        }
    }
}
